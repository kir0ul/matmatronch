# How to download all the pictures of a user on AE's SAS ?

## Setup

1. Get [`Node.js`](https://nodejs.org/en/download)
2. `cd matmatronch`
3. Run `npm install`

## Config

Rename the file `example.config.json` to `config.json`, and modify it with your login, password, and the user's URL you want to download all the pictures from.

## Download

Run `npm start`
