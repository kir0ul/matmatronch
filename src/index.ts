import * as fs from 'fs';
import * as puppeteer from 'puppeteer';

// Get user's vars
const configFile = fs.readFileSync('./config.json', 'utf-8');
const CONFIG = JSON.parse(configFile);
const ROOTURL = 'https://ae.utbm.fr';

async function main() {
  // Open the browser
  const browser = await puppeteer.launch({headless: false});
  const page = await browser.newPage();

  // Login
  await page.goto(ROOTURL + '/login/', {waitUntil: 'networkidle0'}); // wait until page load
  await page.type('#id_username', CONFIG.username);
  await page.type('#id_password', CONFIG.password);
  await Promise.all([
    await page.evaluate(() => {
      // Select the submit button and click on it
      const input = document.querySelector(
        'form[action="/login/"] > input[type="submit"]'
      ) as HTMLInputElement | null;
      if (input !== null) {
        input.click();
      }
    }),
    page.waitForNavigation({waitUntil: 'networkidle0'}),
  ]);
  console.log('Login successful');

  // Go to user's URL
  await page.goto(CONFIG.url, {
    waitUntil: 'networkidle0',
  }); // wait until page load

  // Get all pictures URLs
  await page.evaluate(() => {
    // ToDo: How to access from global variable from outside of the function ??
    const ROOTURL = 'https://ae.utbm.fr';
    const picture_urls: string[] = [];
    const links = document.querySelectorAll('a[href*="sas/picture"]');
    for (let i = 0, max = links.length; i < max; i++) {
      const currentUrl = links[i] as HTMLImageElement;
      const correctUrl =
        ROOTURL +
        String(currentUrl.attributes[0]['nodeValue']).replace('/thumb/', '/');
      picture_urls.push(correctUrl);
      console.log(correctUrl);
    }
    // Follow each picture's URL
    console.log(picture_urls);
  });
  // await page.goto(picture_urls[10], {
  //   waitUntil: 'networkidle0',
  // }); // wait until page load

  // await browser.close()
}

main();
